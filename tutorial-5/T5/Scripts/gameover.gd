extends LinkButton

export(String) var scene_to_load

func _on_Main_Menu_Game_pressed():
	global.lives = 3
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
